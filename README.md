# Support for Micorel MIC&LIGHT


## Fireware Upgrade
![](./img/upgrade.png)

1. Connect the device to a computer.
2. Press and hold the button on the device for 10 seconds.
3. A disk drive named UF2 will be mounted on the computer.
4. Copy the latest `.uf2` file to the disk drive.

If you have any questions, please [open an issue here](https://gitlab.com/micorel/support/-/issues).
